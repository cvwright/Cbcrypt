# Cbcrypt

C language implementation of the BCrypt password hashing function, packaged
with the Swift Package Manager (SPM) for use with the Swift package BCrypt.

Based on PerfectBCrypt from Alexander Berkunov.

## Requirements

- iOS 10.0+
- Xcode 10.1+
- Swift 4.2+

## License

BCrypt is available under the MIT license. See the [LICENSE file](./LICENSE) for more info.
