// swift-tools-version: 5.6

import PackageDescription

let package = Package(
    name: "Cbcrypt",
    products: [
        .library(
            name: "Cbcrypt",
            targets: ["Cbcrypt"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "Cbcrypt",
            dependencies: []),
    ]
)
